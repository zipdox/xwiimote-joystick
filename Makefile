CC=gcc
FLAGS=`pkg-config --cflags libxwiimote`
LIBS=`pkg-config --libs libxwiimote` -lm

xwiimote-joystick: main.c joystick.c
	$(CC) $(FLAGS) -o $@ $^ $(LIBS)
