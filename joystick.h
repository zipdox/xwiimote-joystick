int joystick_create();

void joystick_abs(int fd, int x, int y, int z);

void joystick_btn(int fd, int btn, int state);

void joystick_close(int fd);
