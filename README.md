# xwiimote-joystick

Joystick driver vor xwiimote using uinput. Maps all buttons, and the accelerometer as a 3-axis analog stick.

# Building

All you need is gcc, make, and libxwiimote-dev.

`make`

# Usage

Connect your Wiimote using Bluetooth. Then simply run `xwiimote-joystick`. If you want want to rotate the D-pad for horizontal usage, run `xwiimote-joystick 1`.
