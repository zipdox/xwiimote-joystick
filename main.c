#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/uinput.h>
#include <poll.h>
#include <xwiimote.h>
#include <math.h>
#include <signal.h>
#include "joystick.h"


#define SQR(val) (val * val)
#define NORMALIZE(val) (val < -100 ? -100 : (val > 100 ? 100 : val))

volatile bool running = true;
void int_handler(int sig){
    running = false;
}

int horizontal = 0;
int xoffset, yoffset, zoffset;

void calibrate(struct xwii_iface *dev, struct pollfd *fds){
    int orig_opts = fcntl(STDIN_FILENO, F_GETFL);
    fcntl(STDIN_FILENO, F_SETFL, orig_opts | O_NONBLOCK);
    setvbuf(stdout, NULL, _IONBF, 0);

    char read_char;
    struct xwii_event ev;
    int calibration = 0;
    int xcal = 0, ycal = 0, zcal = 0;
    bool wait = true;

    bool calibrating = true;
    while(calibrating && running){
        poll(fds, 1, 100);
        while(xwii_iface_dispatch(dev, &ev, sizeof(ev)) == 0 && running){
            if(ev.type == XWII_EVENT_ACCEL){
                if(calibration < 10){
                    calibration++;
                    if(calibration < 10) continue;
                    printf("Place your wiimote right-side up on a flat surface and press return");
                }else if(calibration < 20){
                    while(read(STDIN_FILENO, &read_char, 1) > 0){
                        if(read_char == '\n') wait = false;
                    }
                    if(wait) continue;
                    xcal += ev.v.abs[0].x;
                    ycal += ev.v.abs[0].y;
                    zcal += ev.v.abs[0].z;
                    calibration++;
                    if(calibration < 20) continue;
                    printf("Place your wiimote upside-down up on a flat surface and press return");
                    wait = true;
                }else if(calibration < 30){
                    while(read(STDIN_FILENO, &read_char, 1) > 0){
                        if(read_char == '\n') wait = false;
                    }
                    if(wait) continue;
                    xcal += ev.v.abs[0].x;
                    ycal += ev.v.abs[0].y;
                    zcal += ev.v.abs[0].z;
                    calibration++;
                    if(calibration < 30) continue;
                    
                    xoffset = round(xcal / 20.0); 
                    yoffset = round(ycal / 20.0);
                    zoffset = round(zcal / 20.0);
                }else{
                    printf("Calibration complete\n");
                    calibrating = false;
                    break;
                }
            }
        }
    }

    fcntl(STDIN_FILENO, F_SETFL, orig_opts);
}

unsigned int keymap_h[] = {
    3, 2, 0, 1, 4, 5, 6, 7, 8, 9, 10
};

void run(struct xwii_iface *dev, struct pollfd *fds, int joy_fd){
    struct xwii_event ev;
    while(running){
        poll(fds, 1, 100);
        while(xwii_iface_dispatch(dev, &ev, sizeof(ev)) == 0){
            if(ev.type == XWII_EVENT_ACCEL){
                // printf("x: %d, y: %d, z: %d\n", ev.v.abs[0].x - xoffset, ev.v.abs[0].y - yoffset, ev.v.abs[0].z - zoffset);
                joystick_abs(joy_fd, NORMALIZE(ev.v.abs[0].x - xoffset), NORMALIZE(ev.v.abs[0].y - yoffset), NORMALIZE(ev.v.abs[0].z - zoffset));
            }else if(ev.type == XWII_EVENT_KEY){
                // printf("key %d, %d\n", ev.v.key.code, ev.v.key.state);
                if(horizontal){
                    joystick_btn(joy_fd, keymap_h[ev.v.key.code], ev.v.key.state);
                }else{
                    joystick_btn(joy_fd, ev.v.key.code, ev.v.key.state);
                }
            }
        }
    }
}

int main(int argc, char *argv[]){
    signal(SIGINT, int_handler);

    struct xwii_monitor *mon = xwii_monitor_new(false, false);
    if(!mon){
        fprintf(stderr, "Cannot create monitor\n");
        return 1;
    }

    char *ent = xwii_monitor_poll(mon);

    if(!ent){
        fprintf(stderr, "Unable to find wiimote\n");
        return 1;
    }

    printf("Found wiimote: %s\n", ent);

    xwii_monitor_unref(mon);

    struct xwii_iface *dev;

    int success = xwii_iface_new(&dev, ent);
    free(ent);
    if(success != 0){
        fprintf(stderr, "Cannot create interface\n");
        return 1;
    }

    int fd = xwii_iface_get_fd(dev);
    struct pollfd fds = {
        fd,
        POLLIN,
        0
    };

    if(xwii_iface_open(dev, XWII_IFACE_CORE | XWII_IFACE_ACCEL) != 0){
        fprintf(stderr, "Cannot open interface\n");
        xwii_iface_unref(dev);
        return 1;
    }

    int joy_fd = joystick_create();
    if(joy_fd < 0){
        fprintf(stderr, "Cannot open uinput\n");
        xwii_iface_unref(dev);
        return 1;
    }

    if(argc >= 2) if(strcmp(argv[1], "1") == 0) horizontal = 1;
    printf("Starting in %s mode\n", (char*[]){"vertical", "horizontal"}[horizontal]);

    calibrate(dev, &fds);

    if(running) run(dev, &fds, joy_fd);

    joystick_close(joy_fd);
    xwii_iface_unref(dev);
    puts("");

    return 0;
}
