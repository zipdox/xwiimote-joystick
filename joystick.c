
#include <linux/uinput.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

static void setup_abs(int fd, unsigned int chan, int min, int max){
    ioctl(fd, UI_SET_ABSBIT, chan);
  
    struct uinput_abs_setup s = {
        chan,
        {
            .minimum = min,
            .maximum = max
        }
    };

    ioctl(fd, UI_ABS_SETUP, &s);
}

int joystick_create(){
    int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(fd < 0) return fd;

    // Enable button/key handling
    ioctl(fd, UI_SET_EVBIT, EV_KEY);

    // A and B
    ioctl(fd, UI_SET_KEYBIT, BTN_A);
    ioctl(fd, UI_SET_KEYBIT, BTN_B);

    // D-pad
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_UP);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_DOWN);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_LEFT);
    ioctl(fd, UI_SET_KEYBIT, BTN_DPAD_RIGHT);

    // Home, plus, and minus
    ioctl(fd, UI_SET_KEYBIT, BTN_START);
    ioctl(fd, UI_SET_KEYBIT, BTN_THUMBL);
    ioctl(fd, UI_SET_KEYBIT, BTN_THUMBR);

    // 1 and 2
    ioctl(fd, UI_SET_KEYBIT, BTN_1);
    ioctl(fd, UI_SET_KEYBIT, BTN_2);

    // Enable absolute position handling
    ioctl(fd, UI_SET_EVBIT, EV_ABS);

    setup_abs(fd, ABS_X,  -100, 100);
    setup_abs(fd, ABS_Y,  -100, 100);
    setup_abs(fd, ABS_Z,  -100, 100);

    struct uinput_setup setup = {
        .name = "Wiimote",
        .id = {
            .bustype = BUS_BLUETOOTH,
            .vendor  = 0x3,
            .product = 0x3,
            .version = 2,
        }
    };
    ioctl(fd, UI_DEV_SETUP, &setup);
    ioctl(fd, UI_DEV_CREATE);

    return fd;
}

void joystick_abs(int fd, int x, int y, int z){
    struct input_event ev[4];
    memset(&ev, 0, sizeof ev);

    ev[0].type = EV_ABS;
    ev[0].code = ABS_X;
    ev[0].value = x;
    ev[1].type = EV_ABS;
    ev[1].code = ABS_Y;
    ev[1].value = y;
    ev[2].type = EV_ABS;
    ev[2].code = ABS_Z;
    ev[2].value = z;
    ev[3].type = EV_SYN;
    ev[3].code = SYN_REPORT;
    ev[3].value = 0;

    write(fd, &ev, sizeof ev);
}

static unsigned int btn_map[] = {
    BTN_DPAD_LEFT,
    BTN_DPAD_RIGHT,
    BTN_DPAD_UP,
    BTN_DPAD_DOWN,
    BTN_A,
    BTN_B,
    BTN_THUMBL,
    BTN_THUMBR,
    BTN_START,
    BTN_1,
    BTN_2
};

void joystick_btn(int fd, int btn, int state){
    struct input_event ev[2];

    ev[0].type = EV_KEY;
    ev[0].code =  btn_map[btn];
    ev[0].value = state;
    ev[1].type = EV_SYN;
    ev[1].code = SYN_REPORT;
    ev[1].value = 0;

    write(fd, ev, sizeof ev);
}

void joystick_close(int fd){
    ioctl(fd, UI_DEV_DESTROY);
    close(fd);
}
